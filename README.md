## Binar Challenge Collection

### Binar Studi Independen Kampus Merdeka - Backend JavaScript

#### Challenge Link Collection

- Challenge - Chapter 1 - CLI Calculator: [GitLab](https://gitlab.com/raprmdn/binar-challenge/-/tree/master/chapter1) / [GitHub](https://github.com/raprmdn/binar-challenge/tree/master/chapter1)
- Challenge - Chapter 2 - Program Input Nilai Siswa: [GitLab](https://gitlab.com/raprmdn/binar-challenge/-/tree/master/chapter2) / [GitHub](https://github.com/raprmdn/binar-challenge/tree/master/chapter2)
- Challenge - Chapter 3 - Merancang ERD, Database, dan CRUD DML: [GitLab](https://gitlab.com/raprmdn/binar-challenge/-/tree/master/chapter3) / [GitHub](https://github.com/raprmdn/binar-challenge/tree/master/chapter3)
- Challenge - Chapter 4 - RESTful API with ExpressJS: [GitLab](https://gitlab.com/raprmdn/binar-challenge/-/tree/master/chapter4) / [GitHub](https://github.com/raprmdn/binar-challenge/tree/master/chapter4)
- Challenge - Chapter 5 - Postman Collection API Documentation: [GitLab](https://gitlab.com/raprmdn/binar-challenge/-/tree/master/chapter5) / [GitHub](https://github.com/raprmdn/binar-challenge/tree/master/chapter5)


